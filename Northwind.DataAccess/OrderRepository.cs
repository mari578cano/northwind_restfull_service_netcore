﻿using Dapper;
using Dapper.Contrib.Extensions;
using Northwind.Models;
using Northwind.Repositories;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Northwind.DataAccess
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(string _connectionString) : base(_connectionString)
        {      
        }

        ///<summary>
        ///Method for pagination Orders.
        ///</summary>
        ///<return>
        ///Returns a IEnumerable OrderList type object.
        ///</return>
        ///<param name="page">
        ///Number pages to consult.
        ///</param>
        ///<param name="rows">
        ///Number rows to consult.
        ///</param>
        public IEnumerable<OrderList> OrderPagedList(int page, int rows)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@page", page);
            parameters.Add("@rows", rows);

            using (var connection = new SqlConnection(_connectionString))
            {
                var reader = connection.QueryMultiple("dbo.OrderPagedList",
                    parameters, commandType: CommandType.StoredProcedure);

                var orderList = reader.Read<OrderList>().ToList();
                var orderItemList = reader.Read<OrderItemList>().ToList();

                foreach (var item in orderList) item.SetDetails(orderItemList);

                return orderList;
            }
        }

        ///<summary>
        ///Method to get an order for your Id.
        ///</summary>
        ///<return>
        ///Returns an object of type orderList.
        ///</return>
        ///<param name="orderId">
        ///Number order to consult.
        ///</param>
        public OrderList GetOrderById(int orderId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@orderId", orderId);

            using (var connection = new SqlConnection(_connectionString))
            {
                var reader = connection.QueryMultiple("dbo.get_orders_by_id",
                    parameters, commandType: CommandType.StoredProcedure);

                var orderList = reader.Read<OrderList>().ToList();
                var orderItemList = reader.Read<OrderItemList>().ToList();

                foreach (var item in orderList) item.SetDetails(orderItemList);

                return orderList.FirstOrDefault();
            }
        }

    }
}
