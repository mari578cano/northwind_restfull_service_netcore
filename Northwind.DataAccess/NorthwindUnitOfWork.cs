﻿using Northwind.Repositories;
using Northwind.UnitOfWork;

namespace Northwind.DataAccess
{
    public class NorthwindUnitOfWork : IUnitOfWork
    {
        ///<summary>
        ///Variable of type ICustomerRepository containing definitions of the interface.
        ///</summary>
        public ICustomerRepository Customer { get; private set; }

        ///<summary>
        ///Variable of type IUserRepository containing definitions of the interface.
        ///</summary>
        public IUserRepository User { get; private set; }

        ///<summary>
        ///Variable of type ISupplierRepository containing definitions of the interface.
        ///</summary>
        public ISupplierRepository Supplier { get; private set; }

        ///<summary>
        ///Variable of type IOrderRepository containing definitions of the interface.
        ///</summary>
        public IOrderRepository Order { get; private set; }

        ///<summary>
        ///Class constructor.
        ///</summary>
        ///<return>
        ///Returns the objects Customer, User, Supplier and Order for each instance.
        ///</return>
        ///<param name="connectionString">
        ///Connection string.
        ///</param>
        public NorthwindUnitOfWork(string connectionString)
        {
            Customer = new CustomerRepository(connectionString);
            User = new UserRepository(connectionString);
            Supplier = new SupplierRepository(connectionString);
            Order = new OrderRepository(connectionString);
        }
    }
}
