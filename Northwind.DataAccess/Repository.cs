﻿using Dapper.Contrib.Extensions;
using Northwind.Repositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Northwind.DataAccess
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected string _connectionString;

        public Repository(string connectionString)
        {
            SqlMapperExtensions.TableNameMapper = (type) => { return $"{ type.Name }"; };
            _connectionString = connectionString;
        }

        ///<summary>
        ///Method generic for delete.
        ///</summary>
        ///<return>
        ///Returns true if the entity was deleted correctly or false in the opposite case.
        ///</return>
        ///<param name="entity">
        ///Receive an entity of any class.
        ///</param>
        public bool Delete(T entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Delete(entity);
            }
        }

        ///<summary>
        ///Method generic for get a Id.
        ///</summary>
        ///<return>
        ///Returns any entity according to the sent ID.
        ///</return>
        ///<param name="id">
        ///Receive an id for consult.
        ///</param>
        public T GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString)) 
            {
                return connection.Get<T>(id);
            }
        }

        ///<summary>
        ///Generic method to get a list of the submitted entity.
        ///</summary>
        ///<return>
        ///Returns a List of submitted entity.
        ///</return>
        public IEnumerable<T> GetList()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<T>();
            }
        }

        ///<summary>
        ///Method generic for insert.
        ///</summary>
        ///<return>
        ///Returns int with the number of records inserted.
        ///</return>
        ///<param name="entity">
        ///Receive an entity of any class.
        ///</param>
        public int Insert(T entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return (int)connection.Insert(entity);
            }
        }

        ///<summary>
        ///Method generic for update.
        ///</summary>
        ///<return>
        ///Returns true if the entity was updated correctly or false in the opposite case.
        ///</return>
        ///<param name="entity">
        ///Receive an entity of any class.
        ///</param>
        public bool Update(T entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Update(entity);
            }
        }
    }
}
