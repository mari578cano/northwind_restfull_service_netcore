﻿using Dapper;
using Northwind.Models;
using Northwind.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace Northwind.DataAccess
{
    public class UserRepository : Repository<User> , IUserRepository
    {
        public UserRepository(string connectionString) : base(connectionString)
        {
        }

        ///<summary>
        ///Validate that the username and password exist in the system.
        ///</summary>
        ///<return>
        ///Returns a user type object.
        ///</return>
        ///<param name="email">
        ///User email to validate.
        ///</param>
        ///<param name="password">
        ///User password to validate.
        ///</param>
        public User ValidateUser(string email, string password)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@email", email);
            parameters.Add("@password", password);

            using (var connection = new SqlConnection(_connectionString)) 
            {
                return connection.QueryFirstOrDefault<User>(
                    "dbo.ValidateUser", parameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

    }
}
