﻿using Dapper;
using Northwind.Models;
using Northwind.Repositories;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Northwind.DataAccess
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(string connectionString) : base(connectionString)
        {

        }

        ///<summary>
        ///Method for pagination Customer.
        ///</summary>
        ///<return>
        ///Returns a IEnumerable Customer type object.
        ///</return>
        ///<param name="page">
        ///Number pages to consult.
        ///</param>
        ///<param name="rows">
        ///Number rows to consult.
        ///</param>
        public IEnumerable<Customer> GetPaginedCustomer(int page, int rows)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@page", page);
            parameters.Add("@rows", rows);

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Customer>("dbo.CustomerPagedList", parameters,
                                                  commandType: CommandType.StoredProcedure);
            }

        }
    }
}
