﻿using Northwind.Models;
using System.Collections.Generic;

namespace Northwind.BusinessLogic.Interface
{
    public interface ICustomerLogic
    {
        Customer GetById(int id);
        IEnumerable<Customer> GetList();
        IEnumerable<Customer> GetPaginedCustomer(int page, int rows);
        int Insert(Customer customer);
        bool Update(Customer customer);
        bool Delete(Customer customer);
    }
}
