﻿using Northwind.Models;
using System.Collections.Generic;

namespace Northwind.BusinessLogic.Interface
{
    public interface IOrderLogic
    {
        IEnumerable<OrderList> OrderPagedList(int page, int rows);
        bool Delete(Order entity);
        OrderList GetOrderById(int orderId);
        string GetOrderNumber(int id);
    }
}
