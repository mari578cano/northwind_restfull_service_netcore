﻿using Northwind.Models;
using System.Collections.Generic;

namespace Northwind.BusinessLogic.Interface
{
    public interface ISupplierLogic
    {
        Supplier GetById(int id);
        IEnumerable<Supplier> GetPaginatedSupplier(int page, int rows);
        int Insert(Supplier supplier);
        bool Update(Supplier supplier);
        bool Delete(Supplier supplier);
    }
}
