﻿using Northwind.BusinessLogic.Interface;
using Northwind.Models;
using Northwind.UnitOfWork;
using System.Collections.Generic;
using System.Linq;

namespace Northwind.BusinessLogic.Implementatios
{
    public class OrderLogic : IOrderLogic
    {

        ///<summary>
        ///Private variable of type IUnitOfWork.
        ///</summary>
        ///<remarks>
        ///It should not be modified.
        ///</remarks>
        private readonly IUnitOfWork _unitOfWork;

        ///<summary>
        ///Class constructor.
        ///</summary>
        ///<param name="unitOfWork">
        ///Parameter of type IUnitOfWork
        ///</param>
        public OrderLogic(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        ///<summary>
        ///Method calls UnitOfwork interface to access Order property and Delete method.
        ///</summary>
        ///<param name="entity">
        ///Object of type Order.
        ///</param>
        public bool Delete(Order entity) => _unitOfWork.Order.Delete(entity);

        ///<summary>
        ///Method calls UnitOfwork interface to access Order property and GetOrderById method.
        ///</summary>
        ///<param name="orderId">
        ///Number of order Id for to consult.
        ///</param>
        public OrderList GetOrderById(int orderId) => _unitOfWork.Order.GetOrderById(orderId);

        ///<summary>
        ///Method calls UnitOfwork interface to access Order property and OrderPagedList method.
        ///</summary>
        ///<param name="page">
        ///Number page to consult.
        ///</param>
        ///<param name="rows">
        ///Number rows to consult.
        ///</param>
        public IEnumerable<OrderList> OrderPagedList(int page, int rows) => _unitOfWork.Order.OrderPagedList(page, rows);

        ///<summary>
        ///Method calls UnitOfwork interface to access Order property and GetOrderNumber method.
        ///</summary>
        ///<param name="orderId">
        ///Number of order Id for to consult.
        ///</param>
        public string GetOrderNumber(int orderId) 
        {
            var list = _unitOfWork.Order.GetList();
            var record = list.First(x => x.Id == orderId);
            return record.OrderNumber;
        }

    }
}
