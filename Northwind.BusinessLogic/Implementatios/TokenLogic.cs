﻿using Northwind.BusinessLogic.Interface;
using Northwind.Models;
using Northwind.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Northwind.BusinessLogic.Implementatios
{
    public class TokenLogic : ITokenLogic
    {

        ///<summary>
        ///Private variable of type IUnitOfWork.
        ///</summary>
        ///<remarks>
        ///It should not be modified.
        ///</remarks>
        private readonly IUnitOfWork _unitOfWork;

        ///<summary>
        ///Class constructor.
        ///</summary>
        ///<param name="unitOfWork">
        ///Parameter of type IUnitOfWork
        ///</param>
        public TokenLogic(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        ///<summary>
        ///Method calls UnitOfwork interface to access User property and ValidateUser method.
        ///</summary>
        ///<param name="email">
        ///User email to validate.
        ///</param>
        ///<param name="password">
        ///User password to validate.
        ///</param>
        public User ValidateUser(string email, string password) => _unitOfWork.User.ValidateUser(email, password);
    }
}
