﻿using Northwind.BusinessLogic.Interface;
using Northwind.Models;
using Northwind.UnitOfWork;
using System.Collections.Generic;

namespace Northwind.BusinessLogic.Implementatios
{
    public class CustomerLogic : ICustomerLogic
    {

        ///<summary>
        ///Private variable of type IUnitOfWork.
        ///</summary>
        ///<remarks>
        ///It should not be modified.
        ///</remarks>
        private readonly IUnitOfWork _unitOfWork;

        ///<summary>
        ///Class constructor.
        ///</summary>
        ///<param name="unitOfWork">
        ///Parameter of type IUnitOfWork
        ///</param>
        public CustomerLogic(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        ///<summary>
        ///Method calls UnitOfwork interface to access Customer property and Delete method.
        ///</summary>
        ///<param name="customer">
        ///Object of type Customer.
        ///</param>
        public bool Delete(Customer customer) => _unitOfWork.Customer.Delete(customer);

        ///<summary>
        ///Method calls UnitOfwork interface to access Customer property and GetById method.
        ///</summary>
        ///<param name="id">
        ///Customer Id for to consult.
        ///</param>
        public Customer GetById(int id) => _unitOfWork.Customer.GetById(id);

        ///<summary>
        ///Method calls UnitOfwork interface to access Customer property and GetList method.
        ///</summary>
        public IEnumerable<Customer> GetList() => _unitOfWork.Customer.GetList();

        ///<summary>
        ///Method calls UnitOfwork interface to access Customer property and GetPaginedCustomer method.
        ///</summary>
        ///<param name="page">
        ///Number page to consult.
        ///</param>
        ///<param name="rows">
        ///Number rows to consult.
        ///</param>
        public IEnumerable<Customer> GetPaginedCustomer(int page, int rows) => _unitOfWork.Customer.GetPaginedCustomer(page, rows);

        ///<summary>
        ///Method calls UnitOfwork interface to access Customer property and Insert method.
        ///</summary>
        ///<param name="customer">
        ///Object of type Customer.
        ///</param>
        public int Insert(Customer customer) => _unitOfWork.Customer.Insert(customer);

        ///<summary>
        ///Method calls UnitOfwork interface to access Customer property and Update method.
        ///</summary>
        ///<param name="customer">
        ///Object of type Customer.
        ///</param>
        public bool Update(Customer customer) => _unitOfWork.Customer.Update(customer);
    }
}
