﻿using Northwind.BusinessLogic.Interface;
using Northwind.Models;
using Northwind.UnitOfWork;
using System.Collections.Generic;

namespace Northwind.BusinessLogic.Implementatios
{
    public class SupplierLogic : ISupplierLogic
    {

        ///<summary>
        ///Private variable of type IUnitOfWork.
        ///</summary>
        ///<remarks>
        ///It should not be modified.
        ///</remarks>
        private readonly IUnitOfWork _unitOfWork;

        ///<summary>
        ///Class constructor.
        ///</summary>
        ///<param name="unitOfWork">
        ///Parameter of type IUnitOfWork
        ///</param>
        public SupplierLogic(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        ///<summary>
        ///Method calls UnitOfwork interface to access Supplier property and Delete method.
        ///</summary>
        ///<param name="supplier">
        ///Object of type Supplier.
        ///</param>
        public bool Delete(Supplier supplier) => _unitOfWork.Supplier.Delete(supplier);

        ///<summary>
        ///Method calls UnitOfwork interface to access Supplier property and GetById method.
        ///</summary>
        ///<param name="id">
        ///Supplier Id for to consult.
        ///</param>
        public Supplier GetById(int id) => _unitOfWork.Supplier.GetById(id);

        ///<summary>
        ///Method calls UnitOfwork interface to access Supplier property and GetPaginatedSupplier method.
        ///</summary>
        ///<param name="page">
        ///Number page to consult.
        ///</param>
        ///<param name="rows">
        ///Number rows to consult.
        ///</param>
        public IEnumerable<Supplier> GetPaginatedSupplier(int page, int rows) => 
            _unitOfWork.Supplier.SupplierPagedList(page, rows);

        ///<summary>
        ///Method calls UnitOfwork interface to access Supplier property and Insert method.
        ///</summary>
        ///<param name="supplier">
        ///Object of type Supplier.
        ///</param>
        public int Insert(Supplier supplier) => _unitOfWork.Supplier.Insert(supplier);

        ///<summary>
        ///Method calls UnitOfwork interface to access Supplier property and Update method.
        ///</summary>
        ///<param name="supplier">
        ///Object of type Supplier.
        ///</param>
        public bool Update(Supplier supplier) => _unitOfWork.Supplier.Update(supplier);
    }
}
