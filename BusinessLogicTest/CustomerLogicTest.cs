﻿using BusinessLogicTest.Mocked;
using FluentAssertions;
using Northwind.BusinessLogic.Implementatios;
using Northwind.BusinessLogic.Interface;
using Northwind.Models;
using Northwind.UnitOfWork;
using Xunit;

namespace BusinessLogicTest
{
    public class CustomerLogicTest
    {
        private readonly IUnitOfWork _unitMocked;
        private readonly ICustomerLogic _customerLogic;

        public CustomerLogicTest()
        {
            var unitMocked = new CustomerRepositoryMocked();
            _unitMocked = unitMocked.GetInstance();
            _customerLogic = new CustomerLogic(_unitMocked);
        }

        [Fact]
        public void GetById_Customer_Test() 
        {
            var result = _customerLogic.GetById(1);
            result.Should().NotBeNull();
            result.Id.Should().BeGreaterThan(0);
        }


        [Fact(DisplayName = "[CustomerLogic] Insert")]
        public void Insert_Customer_Test()
        {
            var customer = new Customer 
            {
                Id = 101,
                City = "Medellin",
                Country = "Colombia",
                FirstName = "Mariana",
                LastName = "Cano",
                Phone = "11111"
            };

            var result = _customerLogic.Insert(customer);
            result.Should().Be(101);
        }


        [Fact(DisplayName = "[CustomerLogic] Update")]
        public void Update_Customer_Test()
        {
            var customer = new Customer
            {
                Id = 10,
                City = "Medellin",
                Country = "Colombia",
                FirstName = "Mariana",
                LastName = "Cano",
                Phone = "1117711"
            };

            var result = _customerLogic.Update(customer);
            var currentCustomer = _customerLogic.GetById(10);
            currentCustomer.Should().NotBeNull();
            currentCustomer.Id.Should().Be(customer.Id);
            currentCustomer.City.Should().Be(customer.City);
            currentCustomer.Country.Should().Be(customer.Country);
            currentCustomer.FirstName.Should().Be(customer.FirstName);
            currentCustomer.LastName.Should().Be(customer.LastName);
            currentCustomer.Phone.Should().Be(customer.Phone);
        }

    }
}
