﻿using Microsoft.IdentityModel.Tokens;
using Northwind.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;

namespace Northwind.WebApi.Authentication
{
    public class JwtProvider : ITokenProvider
    {

        ///<summary>
        ///Private variable of type RsaSecurityKey.
        ///</summary>
        private RsaSecurityKey _key;
        ///<summary>
        ///Private variable of type string.
        ///</summary>
        private string _algoritm;
        ///<summary>
        ///Private variable of type string.
        ///</summary>
        private string _issuer;
        ///<summary>
        ///Private variable of type string.
        ///</summary>
        private string _audience;

        ///<summary>
        ///Class constructor.
        ///</summary>
        ///<param name="issuer">
        ///Parameter of type string
        ///</param>
        ///<param name="audience">
        ///Parameter of type string
        ///</param>        
        ///<param name="KeyName">
        ///Parameter of type string
        ///</param>
        public JwtProvider(string issuer, string audience, string KeyName)
        {
            var parameters = new CspParameters() { KeyContainerName = KeyName};
            var provider = new RSACryptoServiceProvider(2048, parameters);
            _key = new RsaSecurityKey(provider);
            _algoritm = SecurityAlgorithms.RsaSha256Signature;
            _issuer = issuer;
            _audience = audience;
        }

        ///<summary>
        ///Method in charge of creating the user's token when accessing the system.
        ///</summary>
        ///<return>
        ///Returns the token if the validation process was successful.
        ///</return>
        ///<param name="user">
        ///Receive a User.
        ///</param>
        ///<param name="expiry">
        ///Receive the time it expires.
        ///</param>
        public string CreateToken(User user, DateTime expiry)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

            var identity = new ClaimsIdentity(new List<Claim>() {
                new Claim(ClaimTypes.Name, $"{user.FirstName} {user.LastName}"),
                new Claim(ClaimTypes.Role, user.Roles),
                new Claim(ClaimTypes.PrimarySid, user.Id.ToString())
            }, "Custom");

            SecurityToken token = tokenHandler.CreateJwtSecurityToken(new SecurityTokenDescriptor
            {
                Audience = _audience,
                Issuer = _issuer,
                SigningCredentials = new SigningCredentials(_key, _algoritm),
                Expires = expiry.ToUniversalTime(),
                Subject = identity
            });

            return tokenHandler.WriteToken(token);
        }

        ///<summary>
        ///Method responsible for validating the token.
        ///</summary>
        ///<return>
        ///Returns an object of type TokenValidationParameters.
        ///</return>
        public TokenValidationParameters GetValidationParameters()
        {
            return new TokenValidationParameters
            {
                IssuerSigningKey = _key,
                ValidAudience = _audience,
                ValidIssuer = _issuer,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.FromSeconds(0)
            };
        }
    }
}
