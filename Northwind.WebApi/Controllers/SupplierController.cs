﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Northwind.BusinessLogic.Interface;
using Northwind.Models;

namespace Northwind.WebApi.Controllers
{
    [Route("api/Supplier")]
    [Authorize]
    public class SupplierController : ControllerBase
    {
        private readonly ISupplierLogic _supplierLogic;
        public SupplierController(ISupplierLogic supplierLogic)
        {
            _supplierLogic = supplierLogic;
        }

        [HttpGet]
        [Route("{id:int}")]
        public IActionResult GetById(int id) 
        {
            return Ok(_supplierLogic.GetById(id));
        }

        [HttpGet]
        [Route("GetPaginatedSupplier/{page:int}/{rows:int}")]
        public IActionResult GetPaginatedSupplier(int page, int rows)
        {
            return Ok(_supplierLogic.GetPaginatedSupplier(page, rows));
        }

        [HttpPost]
        public IActionResult Post([FromBody]Supplier supplier)
        {
            if (ModelState.IsValid)
                return Ok(_supplierLogic.Insert(supplier));
            else
                return BadRequest();
        }

        [HttpPut]
        public IActionResult Put([FromBody]Supplier supplier)
        {
            if (ModelState.IsValid && _supplierLogic.Update(supplier))
                return Ok(new { Message = "The supplier is update" });
            else
                return BadRequest();
        }

        [HttpDelete]
        public IActionResult Delete([FromBody]Supplier supplier)
        {
            if (supplier.Id > 0)
                return Ok(_supplierLogic.Delete(supplier));
            else
                return BadRequest();
        }

    }
}