﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Northwind.BusinessLogic.Interface;
using Northwind.Models;
using Northwind.UnitOfWork;

namespace Northwind.WebApi.Controllers
{
    [Route("api/Customer")]
    [Authorize]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerLogic _customerLogic;
        public CustomerController(ICustomerLogic customerLogic)
        {
            _customerLogic = customerLogic;
        }

        [HttpGet]
        [Route("{id:int}")]
        public IActionResult GetById(int id)
        {
            return Ok(_customerLogic.GetById(id));
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_customerLogic.GetList());
        }

        [HttpGet]
        [Route("GetPagenedCustomer/{page:int}/{rows:int}")]
        public IActionResult GetPaginedCustomer(int page, int rows)
        {
            return Ok(_customerLogic.GetPaginedCustomer(page, rows));
        }

        [HttpPost]
        public IActionResult Post([FromBody]Customer customer)
        {
            if (ModelState.IsValid)
                return Ok(_customerLogic.Insert(customer));
            else
                return BadRequest();
        }

        [HttpPut]
        public IActionResult Put([FromBody]Customer customer)
        {
            if (ModelState.IsValid && _customerLogic.Update(customer))
                return Ok(new { Message = "The customer is update" });
            else
                return BadRequest();
        }

        [HttpDelete]
        public IActionResult Delete([FromBody]Customer customer) 
        {
            if (customer.Id > 0)
                return Ok(_customerLogic.Delete(customer));
            else
                return BadRequest(); 
        }

    }
}