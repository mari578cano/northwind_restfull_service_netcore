﻿using Microsoft.AspNetCore.Mvc;
using Northwind.BusinessLogic.Interface;
using Northwind.Models;
using Northwind.UnitOfWork;
using Northwind.WebApi.Authentication;
using System;

namespace Northwind.WebApi.Controllers
{
    [Route("api/Token")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly ITokenProvider _tokenProvider;
        private readonly ITokenLogic _tokenLogic;

        public TokenController(ITokenProvider tokenProvider, ITokenLogic tokenLogic)
        {
            _tokenProvider = tokenProvider;
            _tokenLogic = tokenLogic;
        }

        [HttpPost]
        public JsonWebToken Post([FromBody]User userLogin)
        {
            var user = _tokenLogic.ValidateUser(userLogin.Email, userLogin.Password);

            if (user == null)
                throw new UnauthorizedAccessException();

            var token = new JsonWebToken
            {
                Acces_Token = _tokenProvider.CreateToken(user, DateTime.UtcNow.AddHours(3)),
                Expires_in = 180 //minutes

            };

            return token;
        }

    }
}