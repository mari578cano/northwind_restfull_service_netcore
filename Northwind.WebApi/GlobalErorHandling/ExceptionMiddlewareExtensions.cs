﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Northwind.WebApi.GlobalErorHandling
{
    public static class ExceptionMiddlewareExtensions
    {

        ///<summary>
        ///server exception settings.
        ///</summary>
        ///<return>
        ///Gets the error from the server and the message an object of type ErrorDetails
        ///</return>
        ///<param name="app">
        ///receives a parameter of type IApplicationBuilder, requests from the app.
        ///</param>
        public static void ConfigureExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";

                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        await context.Response.WriteAsync(new ErrorDetails()
                        {
                            StatusCode = context.Response.StatusCode,
                            message = "Internal Server Error."
                        }.ToString());
;                    }
                });
            });
        }

    }
}
